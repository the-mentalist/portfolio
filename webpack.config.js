
const path = require('path');

module.exports = {
    entry: ['./src/index.js'],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    },    
    module: {
        rules:[
            {
            test: /\.js$/,
            use: {
              loader: "babel-loader",
            }
          },
          {
            test: /\.css$/,
            use: [
              {
                loader: "style-loader" // creates style nodes from JS strings
              },
              {
                loader: "css-loader" // translates CSS into CommonJS
              }
            ]
          }
        ]
    }
}