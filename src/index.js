import anime from 'animejs';

$('.intro-text .letters').each(function(){
  $(this).html($(this).text().replace(/([^\x00-\x80]|\w|,)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop: false})
  .add({
    targets: '.intro-text .underline',
    scaleY: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 700
  })
  .add({
    targets: '.intro-text .underline',
    translateX: [0,$(".intro-text .letters").width()+53],
    easing: "easeOutSine",
    duration: 1000,
    delay: 100
  }).add({
    targets: '.intro-text .letter',
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 600,
    offset: '-=1050',
    delay: function(el, i) {
      return 40 * (i+1)
    }
  }).add({
    targets: '.intro-heading',
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1000
  }).add({
    targets: '.intro-scroll-text',
    opacity: [0,1],
    duration: 100,
    offset: '-=1000'
  });

//scroll effect
$('a.intro-scroll-text[href*="#"]').on('click', function(event){
  event.preventDefault();
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top
  }, 500, 'easeInCubic');
});